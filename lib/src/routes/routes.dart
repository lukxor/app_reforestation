import 'package:flutter/material.dart';
import 'package:reforestation_app/src/page/login_page.dart';
import 'package:reforestation_app/src/page/newForm_page.dart';
import 'package:reforestation_app/src/page/sign_up.dart';
import 'package:reforestation_app/src/page/home.dart';
import 'package:reforestation_app/src/page/splash.dart';
import 'package:reforestation_app/src/page/accountSettings_page.dart';
import 'package:reforestation_app/src/page/crmSystem_page.dart';
import 'package:reforestation_app/src/page/profileSearch_page.dart';
import 'package:reforestation_app/src/page/projectManager_page.dart';



Map<String, WidgetBuilder> getApplicationRoutes()
{
  return <String, WidgetBuilder>
  {
    '/'               :(BuildContext context) => LoginPage(),
    'singUp'          :(BuildContext context) => SingUpPage(),
    'home'            :(BuildContext context) => HomePage(),
    'splash'          :(BuildContext context) => SplashPage(),
    'crmSystem'       :(BuildContext context) => CmrSystem(),
    'accountSetting'  :(BuildContext context) => AccountSettings(),
    'newForm'         :(BuildContext context) => NewForm(),
    'profileSearch'   :(BuildContext context) => ProfileSearch(),
    'projectManager'  :(BuildContext context) => ProjectManager(),

  };
}