import 'dart:math' as math;
import 'package:flutter/cupertino.dart';

class Responsive
{
  double width, heigth, inch;

  Responsive(BuildContext context)
  {
    final size = MediaQuery.of(context).size;

    width  = size.width;
    heigth = size.height;

    inch = math.sqrt(math.pow(width, 2) + math.pow(heigth, 2));
  }

  double wp(double percent)
  {
    return width * percent / 100;
  }

  double hp(double percent)
  {
    return heigth * percent / 100;
  }

  double ip(double percent)
  {
    return inch * percent / 100;
  }
}