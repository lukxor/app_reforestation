import 'package:flutter/material.dart';


class InputTex extends StatefulWidget 
{

  final String label;
  final Function(String) validator;
  final bool isSecure;
  final TextInputType inputType;

  const InputTex(
  {
    Key key, 
    @required this.label, 
    this.validator, 
    this.isSecure   = false, 
    this.inputType  = TextInputType.text
  }) : super(key: key);

  @override
  _InputTexState createState() => _InputTexState();
}

class _InputTexState extends State<InputTex> 
{

  @override
  Widget build(BuildContext context) 
  {
    return TextFormField(
      validator   : widget.validator,
      obscureText : widget.isSecure,
      keyboardType: widget.inputType,
      decoration  : InputDecoration(
        labelText     : widget.label,
        contentPadding: EdgeInsets.symmetric(vertical: 10)
      ),
    );
  }
}