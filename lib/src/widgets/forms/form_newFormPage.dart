import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';



class NewFormPage extends StatefulWidget {
  @override
  _NewFormPageState createState() => _NewFormPageState();
}

class _NewFormPageState extends State<NewFormPage> {


  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
     
             Padding(
              padding: const EdgeInsets.all(8.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tight(Size(350, 50)),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Name: *',
                      ),
                    ),
                ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tight(Size(350, 50)),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Last Name: *',
                      ),
                    ),
                ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tight(Size(350, 50)),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Middle Name:',
                      ),
                    ),
                ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tight(Size(350,50)),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Other Last Name (if applicable):',
                      ),
                    ),
                ),
            ),
        
          
        ],
      ),
    );
  }


 
}


    
  