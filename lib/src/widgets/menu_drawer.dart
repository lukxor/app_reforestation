import 'package:flutter/material.dart';

class MenuDrawer extends StatefulWidget 
{
  @override
  _MenuDrawerState createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> 
{
   
  @override
  Widget build(BuildContext context) 
  {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Soporte Stone Web"), 
            accountEmail: null,
            currentAccountPicture: CircleAvatar(
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Color(0xff69f0ae),
                child: CircleAvatar(
                  radius: 40,
                  backgroundImage: AssetImage('assets/avatar04.png'),
                ),
                //child:Image.asset("assets/avatar04.png"),
              ),
            ),
          ),
          ListTile(
            title: Text('New Form'),
            leading: Image.asset("assets/add-icon.ico"),
            onTap: (){
              Navigator.pushNamed(context, "newForm");
            },
          ),
          ListTile(
            title: Text('Web Page'),
            leading: Image.asset("assets/favicon.ico"),
            onTap: (){
               Navigator.pushNamed(context, "home");
            },
          ),
          ListTile(
            title: Text('Home'),
            leading: Image.asset("assets/empresa.ico"),
            onTap: (){
               Navigator.pushNamed(context, "home");
            },
          ),
          ListTile(
            title: Text('Profile Search'),
            leading: Image.asset("assets/Search.ico"),
            onTap: (){
               Navigator.pushNamed(context, "profileSearch");
            },
          ),
          ListTile(
            title: Text('Project Manager'),
            leading: Image.asset("assets/gestion.ico"),
            onTap: (){
               Navigator.pushNamed(context, "projectManager");
            },
          ),
          ListTile(
            title: Text('CRM System'),
            leading: Image.asset("assets/crm2.ico"),
            onTap: (){
                Navigator.pushNamed(context, "crmSystem");
            },
          ),
          ListTile(
            title: Text('Account Settings'),
            leading: Image.asset("assets/account.ico"),
            onTap: (){
               Navigator.pushNamed(context, "accountSetting");
            },
          ),
        ],
      ),
    );
  }
}