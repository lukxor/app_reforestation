import 'package:flutter/material.dart';

import 'package:reforestation_app/src/routes/routes.dart';

void main() 
{
  runApp(MyApp());
}

class MyApp extends StatelessWidget 
{
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) 
  {
    return MaterialApp(

       theme: ThemeData(
        primaryColor: Color(0xFF33691E),      
      ),
      
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: getApplicationRoutes(),
    );
     
    
  }
}

