import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'package:reforestation_app/src/widgets/circle.dart';
import 'package:reforestation_app/src/widgets/input_text.dart';
import 'package:reforestation_app/src/api/auth_api.dart';
import 'package:reforestation_app/src/utils/responsive.dart';



class LoginPage extends StatefulWidget 
{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> 
{

  final _formkey = GlobalKey<FormState>();
  final _authAPI = AuthAPI();

  var _email = '', _password = '';
  var _isFetching = false;

  @override
  void initState()
  {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  }

  //funcion que permite la validacion de los campos email y password
  _submit() async
  {

    if(_isFetching) return;
    {
      final isvalid = _formkey.currentState.validate();

      if(isvalid)
      {
        setState(() {
          _isFetching = true;
        });

        final isOk = await _authAPI.login(context, email: _email, password: _password);

        setState(() {
          _isFetching = false;
        });

        if(isOk)
        {
          print("LOGIN OK");
          Navigator.pushNamed(context, 'home');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) 
  {
    final size = MediaQuery.of(context).size;
    final responsive = Responsive(context);
    

    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: <Widget>[

            Positioned(
              right: -size.width * 0.15,
              top: -size.width * 0.3,
              child: Circle(
                radius: size.width * 0.40,
                colors: [
                  Colors.green[900],
                  Colors.blue[800],
                ],
              ),
            ),
            Positioned(
              left: -size.width * 0.15,
              top: -size.width * 0.30,
              child: Circle(
                radius: size.width * 0.30,
                colors: [
                  Colors.blue[800],
                  Colors.green[900]
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                width: size.width,
                height: size.height,
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            child:Image.asset("assets/logo.png"), 
                            width: responsive.wp(50.0),
                            height: 100,
                            margin: EdgeInsets.only(top: size.width * 0.3),
                          ),
                          SizedBox(height: responsive.hp(6.0)),
                          Text(
                            "Login In",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: responsive.ip(3.5),
                              fontWeight: FontWeight.w400,
                              color: Colors.green[800],
                            ),
                          )
                        ],
                      ),

                      Column(
                        children: <Widget>[
                          ConstrainedBox(
                            constraints:BoxConstraints(
                              maxWidth: 300,
                              minWidth: 300,
                            ),
                            child: Form(
                              key: _formkey ,
                              child: Column(
                                children: <Widget>[
                                  InputTex(
                                    label: "EMAIL ADDRESS",
                                    inputType: TextInputType.emailAddress ,
                                    validator:(String text)
                                    {
                                      if(text.contains("@"))
                                      {
                                        _email= text;
                                        return null;
                                      }
                                      return "Invalid Email";
                                    }
                                  ),
                                  
                                  SizedBox(height: responsive.hp(6.0)),
                                  InputTex(
                                    label: "PASSWORD",
                                    isSecure: true,
                                    validator: (String text)
                                    {
                                      if(text.isNotEmpty && text.length > 6)
                                      {
                                        _password= text;
                                        return null;
                                      }
                                      return "Invalid Password" + "/" + "password must be greater than 6 characters";
                                    }
                                  ),
                                ],
                              ),
                            )
                          ),
                          SizedBox(height: responsive.hp(10.0)),
                           //start button Sing in
                          ConstrainedBox(
                            constraints:BoxConstraints(
                              maxWidth: 300,
                              minWidth: 300,
                            ),
                            child: CupertinoButton(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              color: Colors.green[900],
                              borderRadius: BorderRadius.circular(4),
                              onPressed: () => _submit(),
                              child: Text("Sing in",
                                style: TextStyle(fontSize: responsive.ip(2.5)),
                              ),
                            ),
                          ),
                          //start buttin Sing Up
                          SizedBox(height: responsive.hp(1.0)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("",
                                style: TextStyle(fontSize: 16, color: Colors.black54)),
                              CupertinoButton(
                                child: Text("Sing Up",style: TextStyle(fontSize: 16, color:Colors.green[800]),),
                                onPressed: () => Navigator.pushNamed(context, "singUp"),
                              )  
                            ],
                          ),
                          SizedBox(height: responsive.hp(2.0),)
                        ]
                      )
                    ],
                  )
                )
              ),
            ),
            _isFetching?
              Positioned.fill(
                child: Container(
                  color: Colors.black45,
                  child: Center(
                    child: CupertinoActivityIndicator(radius: 15),
                  ),
                ),
              )
            :Container(),
          ],
        ),
      ),
      )

    );
  }
}

