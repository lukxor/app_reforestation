import 'package:flutter/material.dart';
import 'package:reforestation_app/src/widgets/menu_drawer.dart';


class ProjectManager extends StatefulWidget 
{
  @override
  _ProjectManagerState createState() => _ProjectManagerState();
}

class _ProjectManagerState extends State<ProjectManager> 
{
  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Project Manager"),
        ),
        drawer: MenuDrawer()
      ),
    );
  }
}