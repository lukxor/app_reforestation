import 'package:flutter/material.dart';
import 'package:reforestation_app/src/widgets/menu_drawer.dart';



class AccountSettings extends StatefulWidget 
{
  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends State<AccountSettings> 
{

  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Account Settings"),
        ),
        drawer: MenuDrawer()
      ),
    );
  }
}