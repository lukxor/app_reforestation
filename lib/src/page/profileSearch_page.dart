import 'package:flutter/material.dart';
import 'package:reforestation_app/src/widgets/menu_drawer.dart';


class ProfileSearch extends StatefulWidget 
{
  @override
  _ProfileSearchState createState() => _ProfileSearchState();
}

class _ProfileSearchState extends State<ProfileSearch> 
{
  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Profile Search"),
        ),
        drawer: MenuDrawer()
      ),
    );
  }
}