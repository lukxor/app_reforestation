import 'package:flutter/material.dart';
import 'package:reforestation_app/src/widgets/menu_drawer.dart';


class CmrSystem extends StatefulWidget 
{
  @override
  _CmrSystemState createState() => _CmrSystemState();
}

class _CmrSystemState extends State<CmrSystem> 
{

  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("CMR System"),
        ),
        drawer: MenuDrawer()
      ),
    );
  }
}