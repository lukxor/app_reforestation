import 'package:flutter/material.dart';
import 'package:reforestation_app/src/widgets/forms/form_newFormPage.dart';

import 'package:reforestation_app/src/widgets/menu_drawer.dart';

class NewForm extends StatefulWidget 
{
  @override
  _NewFormState createState() => _NewFormState();
}

class _NewFormState extends State<NewForm> 
{

  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("New Form"),
        ),
        drawer: MenuDrawer(),
          body: Container(
            child: Scaffold(
              body: ListView(
                padding: EdgeInsets.all(10.0),
                children: <Widget>[
                 _cardTipo()
                ]
              )  
            ),  
          ),
        ),  
      );
    
  }
  Widget _cardTipo() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0) ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ListTile(
            leading: Image.asset("assets/add-icon.ico"),
            title: Text('Applicant Information for Reforestation Solutions Inc.'),
          ), 
           NewFormPage(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                color: Colors.blue[900],
                padding: EdgeInsets.all(8.0),
                textColor: Colors.white,
                onPressed: () {},
                child: Text('Add New'),
              ),
              
              FlatButton(
                color: Color(0xFF33691E),
                padding: EdgeInsets.all(8.0),
                textColor: Colors.white,
                onPressed: () {
                  /*...*/
                },
                child: 
                Text(
                  "Save Profile",
                ),
                
              ),
            ],
          )
        ],
      ),
    );
  }
}