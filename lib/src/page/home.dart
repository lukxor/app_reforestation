import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:reforestation_app/src/widgets/menu_drawer.dart';


class HomePage extends StatefulWidget 
{

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> 
{

  @override
  Widget build(BuildContext context) 
  {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Reforestation '),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/login.jpg"),
              fit: BoxFit.cover
            ),
          ),
        ),
        drawer: MenuDrawer(),
      ),
    );
  }
}