import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'package:reforestation_app/src/widgets/circle.dart';
import 'package:reforestation_app/src/widgets/input_text.dart';
import 'package:reforestation_app/src/api/auth_api.dart';


class SingUpPage extends StatefulWidget 
{
  @override
  _SingUpPagePageState createState() => _SingUpPagePageState();
}

class _SingUpPagePageState extends State<SingUpPage> 
{

  final _formkey = GlobalKey<FormState>();
  final _authAPI = AuthAPI();
  
  var _username='', _email='', _password='';
  var _isFetching = false;

  @override
  void initState()
  {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  }

  //funcion que permite la validacion de los campos email y password
  _submit() async
  {

    if(_isFetching) return;

    final isValid = _formkey.currentState.validate();

    if(isValid)
    {

      setState(() {
        _isFetching = true;
      });

      final isOk = await _authAPI.register(context, username: _username, email: _email, password: _password);
      
      setState(() {
        _isFetching = false;
      });
      
      if(isOk)
      {
        print("REGISTER");
        Navigator.pushNamed(context, 'login');
      }
    }
  }

  @override
  Widget build(BuildContext context) 
  {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
          children: <Widget>[

            Positioned(
              right: -size.width * 0.15,
              top: -size.width * 0.3,
              child: Circle(
                radius: size.width * 0.40,
                colors: [
                  Colors.green[900],
                  Colors.blue[800],
                ],
              ),
            ),
            Positioned(
              left: -size.width * 0.15,
              top: -size.width * 0.30,
              child: Circle(
                radius: size.width * 0.30,
                colors: [
                  Colors.blue[800],
                  Colors.green[900]
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                width: size.width,
                height: size.height,
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            child:Image.asset("assets/logo.png"), 
                            width: 200,
                            height: 100,
                            margin: EdgeInsets.only(top: size.width * 0.3),
                          ),
                          SizedBox(height: 50),
                          Text(
                            "Register",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.w400,
                              color: Colors.green[800],
                            ),
                          )
                        ],
                      ),

                      Column(
                        children: <Widget>[
                          ConstrainedBox(
                            constraints:BoxConstraints(
                              maxWidth: 300,
                              minWidth: 300,
                            ),
                            child: Form(
                              key: _formkey ,
                              child: Column(
                                children: <Widget>[
                                  InputTex(
                                    label: "USERNAME",
                                    validator:(String text)
                                    {
                                      if(RegExp(r'[a-zA-Z0-9]+$').hasMatch(text))
                                      {
                                        _username = text;
                                        return null;
                                      }
                                      return "Invalid Username";
                                    }
                                  ),
                                  InputTex(
                                    label: "EMAIL ADDRESS",
                                    inputType: TextInputType.emailAddress ,
                                    validator:(String text)
                                    {
                                      if(text.contains("@"))
                                      {
                                        _email = text;
                                        return null;
                                      }
                                      return "Invalid Email";
                                    }
                                  ),
                                  InputTex(
                                    label: "PASSWORD",
                                    isSecure: true,
                                    validator: (String text)
                                    {
                                      if(text.isNotEmpty && text.length >= 6)
                                      {
                                        _password = text;
                                        return null;
                                      }
                                      return "Invalid Password" + "/" + "password must be greater than 6 characters";
                                    }

                                  ),
                                ],
                              ),
                            )
                          ),

                          //start button Sing in
                          SizedBox(height: 40),
                          ConstrainedBox(
                            constraints:BoxConstraints(
                              maxWidth: 300,
                              minWidth: 300,
                            ),
                            child: CupertinoButton(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              color: Colors.green[900],
                              borderRadius: BorderRadius.circular(4),
                              onPressed: () => _submit(),
                              child: Text("Sing Up",
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                          ),

                          //start buttin Sing Up
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("",
                                style: TextStyle(fontSize: 16, color: Colors.black54)),
                              CupertinoButton(
                                child: Text("",style: TextStyle(fontSize: 16, color:Colors.green[800]),),
                                onPressed: (){}
                              )  
                            ],
                          ),
                          SizedBox(height: size.height * 0.08,)
                        ]
                      )
                    ],
                  )
                )
              ),
            ),
            Positioned(
              left: 10,
              top: 10,
              child: SafeArea(
                child: CupertinoButton(
                  padding: EdgeInsets.all(5),
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.black26,
                  child: Icon(
                    Icons.arrow_back, 
                    color: Colors.white
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
              ),
            ),
            //start fetching dialog
            _isFetching?
              Positioned.fill(
                child: Container(
                  color: Colors.black45,
                  child: Center(
                    child: CupertinoActivityIndicator(radius: 15),
                  ),
                ),
              )
            :Container(),
          ],
        ),
      ),
      )

    );
  }
}

